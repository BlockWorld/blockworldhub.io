function escapeHtml(string)
{
	var entityMap =
	{
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};
    return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
    });
}

function outResult()
{
	var $ = jQuery;
	$("button.JSONP.activate").click(function(){
		$("head").append('<script src="./get/?callback=JSONP"></script>');
	});
}

function JSONPput(data)
{
	var $ = jQuery;
	data = $.parseJSON(data);
	data = JSON.stringify(data, null, 4);
	$(".JSONPDISPLAY").html('<pre>' + data + '</pre>');
	$(".JSONPDISPLAY").addClass('textLeft').addClass('border');
}

jQuery(document).ready(function(){
	outResult();
});
