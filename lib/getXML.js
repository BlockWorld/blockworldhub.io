function notComment(object)
{
	for(var i in object)
	{
		return true;
	}
	return false;
}

function recurseXML(xml)
{
	var newXML = {};
	for(var i in xml)
	{
		if(typeof xml[i] === 'object' && xml[i].constructor !== Array && notComment(xml[i]))
		{
			newXML[i] = [];
			if(i !== "@attributes")
				newXML[i][0] = recurseXML(xml[i]);
			else
				newXML[i] = recurseXML(xml[i]);
		}
		
		else if(typeof xml[i] === 'object' && notComment(xml[i]))
		{
			newXML[i] = [];
			for(var j in xml[i])
			{
				if(typeof xml[i][j] === 'object' && notComment(xml[i][j]))
					newXML[i][j] = recurseXML(xml[i][j]);
				
				else if(typeof xml[i][j] === 'string')
					newXML[i][j] = xml[i][j];
			}
		}
		
		else if (typeof xml[i] === 'string')
		{
			newXML[i] = xml[i];
		}
	}
	return newXML;
}

function checkXML(xml)
{
	return recurseXML(xml);
}
