function escapeHtml(string)
{
	var entityMap =
	{
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};
    return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
    });
}

function outResult()
{
	var $ = window.jQuery;
	var xml = $("script.xml").html();
	xml = $.parseJSON(xml);
	var originalxml = $("script.originalxml").html();
	$("div.result").html('<h1 class="green underline">XML Loaded</h1>');
	$("div.result").append("<h2>Original:</h2>");
	$("div.result").append("<pre>" + originalxml + "</pre>");
	$("div.result").append("<br /><br />");
	$("div.result").append("<h2>JSON:</h2>");
	$("div.result").append("<pre>" + escapeHtml(JSON.stringify(xml, null, 4)) + "</pre>");
}

jQuery(document).ready(function( $ ){
	outResult();
});
